#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Nov 09 21 at 20:44
Because neuron signals are weak for this sample, try to use maximum intensity projection pictures. pictures are sorted by position t from 1 to 88.
file naming rules: <project_name>_h01_t<position_t>_c2.tiff
Do gray color transformation on all Maximum intensity projection .tiff files
@huabai
"""

import os
import cv2
import tifffile

# define:
project_folder = r'/Users/hbai/Desktop/CI/21a_25a21a_6'
project_name = '21a-25a-21a-6_Maximum intensity projection'
position_t = 88
input_folder = os.path.join(project_folder, project_name)
output_folder = os.path.join(project_folder, project_name + '_gray')

# check existence of output folder
try:
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
except OSError:
    print('Error:creating directory', output_folder)

#iterate till files in <project_name> folder

for filename in os.listdir(input_folder):
    if filename.endswith(".tiff") or filename.endswith(".tif"):
        # do gray transform
        new_file_data = tifffile.imread(os.path.join(input_folder, filename))
        new_file_data = cv2.cvtColor(new_file_data, cv2.COLOR_BGR2GRAY)
        # save output into result folder
        new_file_name = os.path.splitext(filename)[0] + '_gray.tiff'
        new_file_path = os.path.join(output_folder,new_file_name)
        tifffile.imwrite(new_file_path, new_file_data, photometric='minisblack')
        print('Done', new_file_name)




