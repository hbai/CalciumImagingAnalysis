#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
README:
This script checks for duplicates in CSV files within subfolders of an Analysis folder.
It lists all subfolder names and CSV file names in each subfolder,
checks for duplicates in the 'POSITION_T' column of each CSV file,
and prints the filenames of files with duplicates.

Usage:
    1. Run the script with the Analysis folder path as a command-line argument.
       Example: python3 Dupcheck.py /path/to/Analysis_folder

@author: Hua Bai, ni-lab, March 8 2024
"""

import os
import pandas as pd


def print_conclusion(message):
    # Print the conclusion message in green color
    print(f"\033[92m{message}\033[0m")


def check_duplicates_in_csv_files(root_folder):
    """
    Checks for duplicates in the 'POSITION_T' column of CSV files in the specified root folder.

    Args:
    - root_folder (str): The path to the root folder to search.
    """
    print("Listing subfolders and CSV files:")
    # Flag to check if any duplicates were found
    duplicates_found = False
    # Walk through all folders and files in the root folder
    for folder_name, _, files in os.walk(root_folder):
        for file_name in files:
            # Check if the file has a .csv extension
            if file_name.endswith('.csv'):
                csv_file_path = os.path.join(folder_name, file_name)
                print(f"Checking file: {csv_file_path}")
                try:
                    # Read the CSV file into a pandas DataFrame
                    df = pd.read_csv(csv_file_path)
                    # Check for duplicates in the 'POSITION_T' column
                    duplicate_rows = df[df.duplicated(subset=['POSITION_T'], keep=False)]
                    # If duplicates are found, print the file path and duplicate rows
                    if not duplicate_rows.empty:
                        duplicates_found = True
                        print("  Duplicates found in CSV file:", csv_file_path)
                        print(duplicate_rows)
                    else:
                        print("  No duplicates found in CSV file:", csv_file_path)
                except Exception as e:
                    print(f"Error processing CSV file {csv_file_path}: {e}")

    # Print final conclusion
    if not duplicates_found:
        print_conclusion("All CSV files look good. No duplicates found.")


if __name__ == "__main__":
    import argparse

    # Argument parser setup
    parser = argparse.ArgumentParser(description="Check for duplicates in CSV files.")
    parser.add_argument("root_folder", type=str, help="Path to the root folder containing CSV files.")
    args = parser.parse_args()

    # Check for duplicates in CSV files
    check_duplicates_in_csv_files(args.root_folder)